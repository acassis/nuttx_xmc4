/****************************************************************************
 * arch/arm/src/xmc4/xmc4_i2c.c
 *
 *   Copyright (C) 2013, 2015-2017 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX, Atmel, nor the names of its contributors may
 *    be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/wdog.h>
#include <nuttx/arch.h>
#include <nuttx/semaphore.h>
#include <nuttx/i2c/i2c_master.h>

#include <nuttx/irq.h>
#include <arch/board/board.h>

#include "up_arch.h"

#include "chip/xmc4_pinmux.h"
#include "chip/xmc4_usic.h"

#include "xmc4_gpio.h"
#include "xmc4_i2c.h"
#include "xmc4_usic.h"

/* REVISIT: Missing support for I2C2 master */

#if defined(CONFIG_XMC4_I2C0) || defined(CONFIG_XMC4_I2C1) || \
    defined(CONFIG_XMC4_I2C2) || defined(CONFIG_XMC4_I2C3) || \
    defined(CONFIG_XMC4_I2C4) || defined(CONFIG_XMC4_I2C5)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Interruption flags definition */

#define I2C_ACK_RCVD    USIC_PSR_IICMODE_ACK
#define I2C_INT_ERRORS  (USIC_CCR_DLIEN)
/*
#define I2C_INT_ERRORS  (USIC_PSR_IICMODE_ERR | USIC_PSR_IICMODE_ARL | \
                         USIC_PSR_IICMODE_NACK)
*/
#define I2C_INT_RXRDY   (USIC_PSR_IICMODE_RIF | USIC_PSR_IICMODE_AIF)
#define I2C_INT_TXRDY   (USIC_CCR_TBIEN)
#define I2C_INT_ALL     0xffffffff

/* Type of TDF */

#define XMC_I2C_TDF_MASTER_SEND     0
#define XMC_I2C_TDF_SLAVE_SEND      (1 << 8)
#define XMC_I2C_TDF_MASTER_RCV_ACK  (2 << 8)
#define XMC_I2C_TDF_MASTER_RCV_NACK (3 << 8)
#define XMC_I2C_TDF_MASTER_START    (4 << 8)
#define XMC_I2C_TDF_MASTER_RESTART  (5 << 8)
#define XMC_I2C_TDF_MASTER_STOP     (6 << 8)

/****************************************************************************
 * Configuration
 ****************************************************************************/

#ifndef CONFIG_XMC4_I2C0_FREQUENCY
#  define CONFIG_XMC4_I2C0_FREQUENCY 100000
#endif

#ifndef CONFIG_XMC4_I2C1_FREQUENCY
#  define CONFIG_XMC4_I2C1_FREQUENCY 100000
#endif

#ifndef CONFIG_XMC4_I2C2_FREQUENCY
#  define CONFIG_XMC4_I2C2_FREQUENCY 100000
#endif

#ifndef CONFIG_XMC4_I2C3_FREQUENCY
#  define CONFIG_XMC4_I2C3_FREQUENCY 100000
#endif

#ifndef CONFIG_XMC4_I2C4_FREQUENCY
#  define CONFIG_XMC4_I2C4_FREQUENCY 100000
#endif

#ifndef CONFIG_XMC4_I2C4_FREQUENCY
#  define CONFIG_XMC4_I2C4_FREQUENCY 100000
#endif

#ifndef CONFIG_DEBUG_I2C_INFO
#  undef CONFIG_XMC4_I2C_REGDEBUG
#endif

#define XMC_I2C_OVERSAMPLING    (2UL)

/* Driver internal definitions *************************************************/

#define I2C_TIMEOUT ((100 * CLK_TCK) / 1000) /* 100 mS */

/* Clocking to the TWO module(s) is provided by the main clocked, divided down
 * as necessary.
 */

#define I2C_MAX_FREQUENCY 66000000   /* Maximum I2C frequency */

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct i2c_dev_s
{
  struct i2c_master_s dev;        /* Generic I2C device */
  struct i2c_msg_s    *msg;       /* Message list */
  uintptr_t           base;       /* Base address of registers */
  uint32_t            i2cfreq;    /* Selected I2C frequency */
  uint16_t            irq;        /* IRQ number for this device */
  uint8_t             msgc;       /* Number of message in the message list */
  uint8_t             i2cchan;    /* I2C peripheral channel number */
  uint8_t             dx0;        /* Input source for DX0 */
  uint8_t             dx1;        /* Input source for DX1 */

  sem_t               exclsem;    /* Only one thread can access at a time */
  sem_t               waitsem;    /* Wait for I2C transfer completion */
  WDOG_ID             timeout;    /* Watchdog to recover from bus hangs */
  volatile int        result;     /* The result of the transfer */
  volatile int        xfrd;       /* Number of bytes transfers */

  /* Debug stuff */

#ifdef CONFIG_XMC4_I2C_REGDEBUG
   bool               wrlast;     /* Last was a write */
   uint32_t           addrlast;   /* Last address */
   uint32_t           vallast;    /* Last value */
   int                ntimes;     /* Number of times */
#endif
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Low-level helper functions */

static void i2c_takesem(sem_t *sem);
#define     i2c_givesem(sem) (nxsem_post(sem))

#ifdef CONFIG_XMC4_I2C_REGDEBUG
static bool i2c_checkreg(struct i2c_dev_s *priv, bool wr,
              uint32_t value, uintptr_t address);
static uint32_t i2c_getabs(struct i2c_dev_s *priv, uintptr_t address);
static void i2c_putabs(struct i2c_dev_s *priv, uintptr_t address,
              uint32_t value);
#else
# define    i2c_checkreg(priv,wr,value,address) (false)
# define    i2c_putabs(p,a,v) putreg32(v,a)
# define    i2c_getabs(p,a) getreg32(a)
#endif

static inline uint32_t i2c_getrel(struct i2c_dev_s *priv,
          unsigned int offset);
static inline void i2c_putrel(struct i2c_dev_s *priv, unsigned int offset,
          uint32_t value);

/* I2C transfer helper functions */

static int  i2c_wait(struct i2c_dev_s *priv);
static void i2c_wakeup(struct i2c_dev_s *priv, int result);
static int  i2c_interrupt(int irq, FAR void *context, FAR void *arg);
static void i2c_timeout(int argc, uint32_t arg, ...);

static void i2c_startread(struct i2c_dev_s *priv, struct i2c_msg_s *msg);
static void i2c_startwrite(struct i2c_dev_s *priv, struct i2c_msg_s *msg);
static void i2c_startmessage(struct i2c_dev_s *priv, struct i2c_msg_s *msg);

/* I2C device operations */

static int i2c_transfer(FAR struct i2c_master_s *dev,
          FAR struct i2c_msg_s *msgs, int count);
#ifdef CONFIG_I2C_RESET
static int  i2c_reset(FAR struct i2c_master_s * dev);
#endif

/* Initialization */

static void i2c_setfrequency(struct i2c_dev_s *priv, uint32_t frequency);
static void i2c_hw_initialize(struct i2c_dev_s *priv, uint32_t frequency);

/****************************************************************************
 * Private Data
 ****************************************************************************/

#ifdef CONFIG_XMC4_I2C0
static struct i2c_dev_s g_i2c0;
#endif

#ifdef CONFIG_XMC4_I2C1
static struct i2c_dev_s g_i2c1;
#endif

#ifdef CONFIG_XMC4_I2C2
static struct i2c_dev_s g_i2c2;
#endif

#ifdef CONFIG_XMC4_I2C3
static struct i2c_dev_s g_i2c3;
#endif

#ifdef CONFIG_XMC4_I2C4
static struct i2c_dev_s g_i2c4;
#endif

#ifdef CONFIG_XMC4_I2C5
static struct i2c_dev_s g_i2c5;
#endif

static const struct i2c_ops_s g_i2cops =
{
  .transfer = i2c_transfer
#ifdef CONFIG_I2C_RESET
  , .reset  = i2c_reset
#endif
};

/****************************************************************************
 * Low-level Helpers
 ****************************************************************************/
/****************************************************************************
 * Name: i2c_takesem
 *
 * Description:
 *   Take the wait semaphore (handling false alarm wake-ups due to the receipt
 *   of signals).
 *
 * Input Parameters:
 *   dev - Instance of the SDIO device driver state structure.
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

static void i2c_takesem(sem_t *sem)
{
  int ret;

  do
    {
      /* Take the semaphore (perhaps waiting) */

      ret = nxsem_wait(sem);

      /* The only case that an error should occur here is if the wait was
       * awakened by a signal.
       */

      DEBUGASSERT(ret == OK || ret == -EINTR);
    }
  while (ret == -EINTR);
}

/****************************************************************************
 * Name: i2c_checkreg
 *
 * Description:
 *   Check if the current register access is a duplicate of the preceding.
 *
 * Input Parameters:
 *   value   - The value to be written
 *   address - The address of the register to write to
 *
 * Returned Value:
 *   true:  This is the first register access of this type.
 *   false: This is the same as the preceding register access.
 *
 ****************************************************************************/

#ifdef CONFIG_XMC4_I2C_REGDEBUG
static bool i2c_checkreg(struct i2c_dev_s *priv, bool wr, uint32_t value,
                         uint32_t address)
{
  if (wr      == priv->wrlast &&   /* Same kind of access? */
      value   == priv->vallast &&  /* Same value? */
      address == priv->addrlast)   /* Same address? */
    {
      /* Yes, then just keep a count of the number of times we did this. */

      priv->ntimes++;
      return false;
    }
  else
    {
      /* Did we do the previous operation more than once? */

      if (priv->ntimes > 0)
        {
          /* Yes... show how many times we did it */

          i2cinfo("...[Repeats %d times]...\n", priv->ntimes);
        }

      /* Save information about the new access */

      priv->wrlast   = wr;
      priv->vallast  = value;
      priv->addrlast = address;
      priv->ntimes   = 0;
    }

  /* Return true if this is the first time that we have done this operation */

  return true;
}
#endif

/****************************************************************************
 * Name: i2c_getabs
 *
 * Description:
 *  Read any 32-bit register using an absolute
 *
 ****************************************************************************/

#ifdef CONFIG_XMC4_I2C_REGDEBUG
static uint32_t i2c_getabs(struct i2c_dev_s *priv, uintptr_t address)
{
  uint32_t value = getreg32(address);

  if (i2c_checkreg(priv, false, value, address))
    {
      i2cinfo("%08x->%08x\n", address, value);
    }

  return value;
}
#endif

/****************************************************************************
 * Name: i2c_putabs
 *
 * Description:
 *  Write to any 32-bit register using an absolute address
 *
 ****************************************************************************/

#ifdef CONFIG_XMC4_I2C_REGDEBUG
static void i2c_putabs(struct i2c_dev_s *priv, uintptr_t address,
                       uint32_t value)
{
  if (i2c_checkreg(priv, true, value, address))
    {
      i2cinfo("%08x<-%08x\n", address, value);
    }

  putreg32(value, address);
}
#endif

/****************************************************************************
 * Name: i2c_getrel
 *
 * Description:
 *  Read a I2C register using an offset relative to the I2C base address
 *
 ****************************************************************************/

static inline uint32_t i2c_getrel(struct i2c_dev_s *priv, unsigned int offset)
{
  return i2c_getabs(priv, priv->base + offset);
}

/****************************************************************************
 * Name: i2c_putrel
 *
 * Description:
 *  Write a value to a I2C register using an offset relative to the I2C base
 *  address.
 *
 ****************************************************************************/

static inline void i2c_putrel(struct i2c_dev_s *priv, unsigned int offset,
                              uint32_t value)
{
  i2c_putabs(priv, priv->base + offset, value);
}

/****************************************************************************
 * I2C transfer helper functions
 ****************************************************************************/

/****************************************************************************
 * Name: i2c_wait
 *
 * Description:
 *   Perform a I2C transfer start
 *
 * Assumptions:
 *   Interrupts are disabled
 *
 ****************************************************************************/

static int i2c_wait(struct i2c_dev_s *priv)
{
  /* Start a timeout to avoid hangs */

  (void)wd_start(priv->timeout, I2C_TIMEOUT, i2c_timeout, 1, (uint32_t)priv);

  /* Wait for either the I2C transfer or the timeout to complete */

  do
    {
      i2cinfo("I2C%d Waiting...\n", priv->i2cchan);
      i2c_takesem(&priv->waitsem);
      i2cinfo("I2C%d Awakened with result:%d\n", priv->i2cchan, priv->result);
    }
  while (priv->result == -EBUSY);

  /* We get here via i2c_wakeup.  The watchdog timer has been disabled and
   * all further interrupts for the I2C have been disabled.
   */

  return priv->result;
}

/****************************************************************************
 * Name: i2c_wakeup
 *
 * Description:
 *   A terminal event has occurred.  Wake-up the waiting thread
 *
 ****************************************************************************/

static void i2c_wakeup(struct i2c_dev_s *priv, int result)
{
  /* Cancel any pending timeout */

  wd_cancel(priv->timeout);

  /* Disable any further I2C interrupts */

  i2c_putrel(priv, XMC4_USIC_PSCR_OFFSET, I2C_INT_ALL);

  /* Wake up the waiting thread with the result of the transfer */

  priv->result = result;
  i2c_givesem(&priv->waitsem);
}

/****************************************************************************
 * Name: i2c_interrupt
 *
 * Description:
 *   The I2C Interrupt Handler
 *
 ****************************************************************************/

static int i2c_interrupt(int irq, FAR void *context, FAR void *arg)
{
  struct i2c_dev_s *priv = (struct i2c_dev_s *)arg;
  struct i2c_msg_s *msg;
  uint32_t psr;
  uint32_t pcr;
  uint32_t pending;
  uint32_t regval;

  DEBUGASSERT(priv != NULL);

  /* Retrieve masked interrupt status */

  psr = i2c_getrel(priv, XMC4_USIC_PSR_OFFSET);
  pcr = i2c_getrel(priv, XMC4_USIC_PSR_OFFSET);

  /* Align PCR to meet PSR position */

  pcr = pcr >> 16;

  pending = psr & pcr;

  i2cinfo("I2C%d pending: %08x\n", priv->i2cchan, pending);

  msg = priv->msg;

  /* Check for errors */

  if ((pending & I2C_INT_ERRORS) != 0)
    {
      /* Wake up the thread with an I/O error indication */

      i2cerr("ERROR: I2C%d pending: %08x\n", priv->i2cchan, pending);
      i2c_wakeup(priv, -EIO);
    }

  /* ACK received */

  else if ((pending & I2C_ACK_RCVD) != 0)
    {
      i2cinfo("ACK Received!\n");

      /* Clear ACK interruption */

      i2c_putrel(priv, XMC4_USIC_PSCR_OFFSET, USIC_PSR_IICMODE_ACK);
    }

  /* Byte received */

  else if ((pending & I2C_INT_RXRDY) != 0)
    {
      msg->buffer[priv->xfrd] = i2c_getrel(priv, XMC4_USIC_RBUF_OFFSET);
      priv->xfrd++;

      /* Check for transfer complete */

      if (priv->xfrd >= msg->length)
        {
          /* The transfer is complete.  Clear interrupts. */

          i2c_putrel(priv, XMC4_USIC_PSCR_OFFSET, I2C_INT_RXRDY);

          /* Disable RXRDY interrupts */

          regval = i2c_getrel(priv, XMC4_USIC_PCR_OFFSET);
          regval |= ~I2C_INT_RXRDY;
          i2c_putrel(priv, XMC4_USIC_PCR_OFFSET, regval);

          /* Is there another message to send? */

          if (priv->msgc > 1)
            {
              /* Yes... start the next message */

              priv->msg++;
              priv->msgc--;
              i2c_startmessage(priv, priv->msg);
            }
          else
            {
              /* No.. we made it to the end of the message list with no errors.
               * Cancel any timeout and wake up the waiting thread with a
               * success indication.
               */

              i2c_wakeup(priv, OK);
            }
        }

      /* Not yet complete, but will the next be the last byte? */

      else if (priv->xfrd == (msg->length - 1))
        {
          /* Clear the interruption */

          i2c_putrel(priv, XMC4_USIC_PSCR_OFFSET, I2C_INT_RXRDY);

          /* Yes, set the stop signal */

          i2c_putrel(priv, XMC4_USIC_TBUF_OFFSET, XMC_I2C_TDF_MASTER_STOP);
        }
    }

  /* Byte sent */

  else if ((pending & I2C_INT_TXRDY) != 0)
    {
      /* Transfer finished? */

      if (priv->xfrd >= msg->length)
        {
          /* The transfer is complete.  Clear TXRDY interrupt. */

          i2c_putrel(priv, XMC4_USIC_PSCR_OFFSET, 0xffffffff /*I2C_INT_TXRDY*/);

          /* Disable TXRDY interrupt */

          regval = i2c_getrel(priv, XMC4_USIC_CCR_OFFSET);
          regval |= ~I2C_INT_TXRDY;
          i2c_putrel(priv, XMC4_USIC_CCR_OFFSET, regval);

          /* Send the STOP condition */

          i2c_putrel(priv, XMC4_USIC_TBUF_OFFSET, XMC_I2C_TDF_MASTER_STOP);

          /* Is there another message to send? */

          if (priv->msgc > 1)
            {
              /* Yes... start the next message */

              priv->msg++;
              priv->msgc--;
              i2c_startmessage(priv, priv->msg);
            }
          else
            {
              /* No.. we made it to the end of the message list with no errors.
               * Cancel any timeout and wake up the waiting thread with a
               * success indication.
               */

              i2c_wakeup(priv, OK);
            }
        }

      /* No, there are more bytes remaining to be sent */

      else
        {
          /* Clear the TXRDY interrupt */

          i2c_putrel(priv, XMC4_USIC_PSCR_OFFSET, 0xffffffff /*I2C_INT_TXRDY*/);

          /* Transmit next byte */

          i2c_putrel(priv, XMC4_USIC_TBUF_OFFSET, msg->buffer[priv->xfrd]);
          priv->xfrd++;
        }
    }

  return OK;
}

/****************************************************************************
 * Name: i2c_timeout
 *
 * Description:
 *   Watchdog timer for timeout of I2C operation
 *
 * Assumptions:
 *   Called from the timer interrupt handler with interrupts disabled.
 *
 ****************************************************************************/

static void i2c_timeout(int argc, uint32_t arg, ...)
{
  struct i2c_dev_s *priv = (struct i2c_dev_s *)arg;

  i2cerr("ERROR: I2C%d Timeout!\n", priv->i2cchan);
  i2c_wakeup(priv, -ETIMEDOUT);
}

/****************************************************************************
 * Name: i2c_startread
 *
 * Description:
 *   Start the next read message
 *
 ****************************************************************************/

static void i2c_startread(struct i2c_dev_s *priv, struct i2c_msg_s *msg)
{
  /* Setup for the transfer */

  priv->result = -EBUSY;
  priv->xfrd   = 0;

  /* Set slave address with READ indication bit and send start condition */

  i2c_putrel(priv, XMC4_USIC_TBUF_OFFSET, (msg->addr | I2C_READBIT | XMC_I2C_TDF_MASTER_START));
}

/****************************************************************************
 * Name: i2c_startwrite
 *
 * Description:
 *   Start the next write message
 *
 ****************************************************************************/

static void i2c_startwrite(struct i2c_dev_s *priv, struct i2c_msg_s *msg)
{
  uint32_t regval;

  /* Setup for the transfer */

  priv->result = -EBUSY;
  priv->xfrd   = 0;

  /* Set slave address and send start condition */

  i2c_putrel(priv, XMC4_USIC_TBUF_OFFSET, msg->addr | XMC_I2C_TDF_MASTER_START);
}

/****************************************************************************
 * Name: i2c_startmessage
 *
 * Description:
 *   Start the next write message
 *
 ****************************************************************************/

static void i2c_startmessage(struct i2c_dev_s *priv, struct i2c_msg_s *msg)
{
  if ((msg->flags & I2C_M_READ) != 0)
    {
      i2c_startread(priv, msg);
    }
  else
    {
      i2c_startwrite(priv, msg);
    }
}

/****************************************************************************
 * I2C device operations
 ****************************************************************************/

/****************************************************************************
 * Name: i2c_transfer
 *
 * Description:
 *   Receive a block of data on I2C using the previously selected I2C
 *   frequency and slave address.
 *
 ****************************************************************************/

static int i2c_transfer(FAR struct i2c_master_s *dev,
                        FAR struct i2c_msg_s *msgs, int count)
{
  struct i2c_dev_s *priv = (struct i2c_dev_s *)dev;
  irqstate_t flags;
  int ret;
  uint32_t regval;

  DEBUGASSERT(dev != NULL);
  i2cinfo("I2C%d count: %d\n", priv->i2cchan, count);

  /* Get exclusive access to the device */

  i2c_takesem(&priv->exclsem);

  /* Setup the message transfer */

  priv->msg  = msgs;
  priv->msgc = count;

  /* Configure the I2C frequency */

  i2c_setfrequency(priv, msgs->frequency);

  /* Initiate the transfer.  The rest will be handled from interrupt
   * logic.  Interrupts must be disabled to prevent re-entrance from the
   * interrupt level.
   */

  flags = enter_critical_section();
  i2c_startmessage(priv, msgs);

  /* And wait for the transfers to complete.  Interrupts will be re-enabled
   * while we are waiting.
   */

  ret = i2c_wait(priv);
  if (ret < 0)
    {
      i2cerr("ERROR: Transfer failed: %d\n", ret);
      regval = i2c_getrel(priv, XMC4_USIC_PCR_OFFSET);
      i2cerr("PCR = 0x%08x\n", regval);

      regval = i2c_getrel(priv, XMC4_USIC_PSR_OFFSET);
      i2cerr("PSR = 0x%08x\n", regval);
    }

  leave_critical_section(flags);
  i2c_givesem(&priv->exclsem);
  return ret;
}

/************************************************************************************
 * Name: i2c_reset
 *
 * Description:
 *   Perform an I2C bus reset in an attempt to break loose stuck I2C devices.
 *
 * Input Parameters:
 *   dev   - Device-specific state data
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ************************************************************************************/

#ifdef CONFIG_I2C_RESET
static int i2c_reset(FAR struct i2c_master_s * dev)
{
  return OK;
}
#endif /* CONFIG_I2C_RESET */

/****************************************************************************
 * Initialization
 ****************************************************************************/

/****************************************************************************
 * Name: i2c_setfrequency
 *
 * Description:
 *   Set the frequency for the next transfer
 *
 ****************************************************************************/

static void i2c_setfrequency(struct i2c_dev_s *priv, uint32_t frequency)
{
  int ret;

  DEBUGASSERT(priv != NULL && priv->i2cchan >= 0 && priv->i2cchan < 6);

  if (frequency != priv->i2cfreq)
    {
      /* Set I2C frequency (USIC baudrate) */

      ret = xmc4_usic_baudrate(priv->i2cchan, frequency, XMC_I2C_OVERSAMPLING);
      if (ret < 0)
        {
          i2cerr("Setting frequency to %d failed!\n", frequency);
          return;
        }

      priv->i2cfreq = frequency;
    }
}

/****************************************************************************
 * Name: i2c_hw_initialize
 *
 * Description:
 *   Initialize one I2C peripheral for I2C operation
 *
 ****************************************************************************/

static void i2c_hw_initialize(struct i2c_dev_s *priv, uint32_t frequency)
{
  int ret;
  uint32_t regval;

  i2cinfo("I2C%d Initializing\n", priv->i2cchan);

  /* Enable the USIC channel */

  ret = xmc4_enable_usic_channel(priv->i2cchan);
  if (ret < 0)
    {
      i2cerr("ERROR: Failed to enable USIC channel!\n");
      return;
    }

  /* Put CCR on mode 0 to start configuration */

  i2c_putrel(priv, XMC4_USIC_CCR_OFFSET, 0);

  /* Data format configuration
   *
   * Transmission Mode (TRM)     = 3
   * Passive Data Level (PDL)    = 1
   * Serial Direction MSB (SDIR) = 1
   * Frame Length (FLE)          = 63
   * Word Length (WLE) 8-bit     = 7
   */

  regval  = USIC_SCTR_PDL1 | USIC_SCTR_TRM_ACTIVE | USIC_SCTR_SDIR |
            USIC_SCTR_FLE(64) | USIC_SCTR_WLE(8);
  i2c_putrel(priv, XMC4_USIC_SCTR_OFFSET, regval);

  /* Set DX0CR input source path */

  regval  = i2c_getrel(priv, XMC4_USIC_DX0CR_OFFSET);
  regval &= ~USIC_DXCR_DSEL_MASK;
  regval |= USIC_DXCR_DSEL_DX(priv->dx0);
  i2c_putrel(priv, XMC4_USIC_DX0CR_OFFSET, regval);

  /* Set DX1CR input source path */

  regval  = i2c_getrel(priv, XMC4_USIC_DX1CR_OFFSET);
  regval &= ~USIC_DXCR_DSEL_MASK;
  regval |= USIC_DXCR_DSEL_DX(priv->dx1);
  i2c_putrel(priv, XMC4_USIC_DX1CR_OFFSET, regval);

  /* Set the initial I2C data transfer frequency */

  priv->i2cfreq = 0;
  i2c_setfrequency(priv, frequency);

#if 0
  /* Disable TX FIFO */

  regval  = i2c_getrel(priv, XMC4_USIC_TBCTR_OFFSET);
  regval &= ~(USIC_TBCTR_SIZE_MASK);
  i2c_putrel(priv, XMC4_USIC_TBCTR_OFFSET, regval);

  /* Configure TX FIFO */

  regval  = i2c_getrel(priv, XMC4_USIC_TBCTR_OFFSET);
  regval &= ~(USIC_TBCTR_LIMIT_MASK | USIC_TBCTR_DPTR_MASK | USIC_TBCTR_SIZE_MASK);
  regval |= USIC_TBCTR_LIMIT(15) | USIC_TBCTR_DPTR(16) | USIC_TBCTR_SIZE_16;
  i2c_putrel(priv, XMC4_USIC_TBCTR_OFFSET, regval);
  
  /* Disable RX FIFO */

  regval  = i2c_getrel(priv, XMC4_USIC_RBCTR_OFFSET);
  regval &= ~(USIC_RBCTR_SIZE_MASK);
  i2c_putrel(priv, XMC4_USIC_RBCTR_OFFSET, regval);

  /* Configure RX FIFO */

  regval  = i2c_getrel(priv, XMC4_USIC_RBCTR_OFFSET);
  regval &= ~(USIC_RBCTR_LIMIT_MASK | USIC_RBCTR_DPTR_MASK | USIC_RBCTR_SIZE_MASK | USIC_RBCTR_LOF);
  regval |= USIC_RBCTR_LIMIT(15) | USIC_RBCTR_DPTR(0) | USIC_RBCTR_SIZE_16 | USIC_RBCTR_LOF;
  i2c_putrel(priv, XMC4_USIC_RBCTR_OFFSET, regval);
#endif
  
  /* Setup PCR interrupts */

  regval = i2c_getrel(priv, XMC4_USIC_PCR_OFFSET);
  regval |= USIC_PCR_IICMODE_ARLIEN | USIC_PCR_IICMODE_ERRIEN;
  regval |= USIC_PCR_IICMODE_NACKIEN | USIC_PCR_IICMODE_MCLK;
  regval |= USIC_PCR_IICMODE_ACKIEN | USIC_PCR_IICMODE_SCRIEN;
  i2c_putrel(priv, XMC4_USIC_PCR_OFFSET, regval);

  /* Setup CCR to I2C mode */

  regval = i2c_getrel(priv, XMC4_USIC_CCR_OFFSET);
  regval &= ~USIC_CCR_MODE_MASK;
  regval |= USIC_CCR_MODE_I2C;
  regval |= (USIC_CCR_RIEN | USIC_CCR_AIEN | USIC_CCR_TSIEN | USIC_CCR_TBIEN);
  i2c_putrel(priv, XMC4_USIC_CCR_OFFSET, regval);

  /* Clear status flags */

  i2c_putrel(priv, XMC4_USIC_PSCR_OFFSET, 0xffffffff);

}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: xmc4_i2cbus_initialize
 *
 * Description:
 *   Initialize a I2C device for I2C operation
 *
 ****************************************************************************/

struct i2c_master_s *xmc4_i2cbus_initialize(int channel)
{
  struct i2c_dev_s *priv;
  irqstate_t flags;
  uint32_t frequency;

  i2cinfo("Initializing I2C%d\n", channel);

  flags = enter_critical_section();

#ifdef CONFIG_XMC4_I2C0
  if (channel == 0)
    {
      /* Set up I2C0 register base address and IRQ number */

      priv              = &g_i2c0;
      priv->base        = XMC4_USIC0_CH0_BASE;
      priv->irq         = XMC4_IRQ_USIC0_SR0;
      priv->i2cchan     = 0;
      priv->dx0         = BOARD_I2C0_DX0;
      priv->dx1         = BOARD_I2C0_DX1;

      /* Configure PIO pins */

      xmc4_gpio_config(GPIO_I2C0_SCL);
      xmc4_gpio_config(GPIO_I2C0_SDA);

      /* Select the I2C frequency */

      frequency  = CONFIG_XMC4_I2C0_FREQUENCY;
    }
  else
#endif
#ifdef CONFIG_XMC4_I2C1
  if (channel == 1)
    {
      /* Set up I2C1 register base address and IRQ number */

      priv           = &g_i2c1;
      priv->base     = XMC4_USIC0_CH1_BASE;
      priv->irq      = XMC4_IRQ_USIC0_SR1;
      priv->i2cchan  = 1;
      priv->dx0      = BOARD_I2C1_DX0;
      priv->dx1      = BOARD_I2C1_DX1;

      /* Configure PIO pins */

      xmc4_gpio_config(GPIO_I2C1_SCL);
      xmc4_gpio_config(GPIO_I2C1_SDA);

      /* Select the I2C frequency */

      frequency  = CONFIG_XMC4_I2C1_FREQUENCY;
    }
  else
#endif
#ifdef CONFIG_XMC4_I2C2
  if (channel == 2)
    {
      /* Set up I2C2 register base address and IRQ number */

      priv           = &g_i2c2;
      priv->base     = XMC4_USIC1_CH0_BASE;
      priv->irq      = XMC4_IRQ_USIC1_SR0;
      priv->i2cchan  = 2;
      priv->dx0      = BOARD_I2C2_DX0;
      priv->dx1      = BOARD_I2C2_DX1;

      /* Configure PIO pins */

      xmc4_gpio_config(GPIO_I2C2_SCL);
      xmc4_gpio_config(GPIO_I2C2_SDA);

      /* Select the I2C frequency, and peripheral ID */

      frequency  = CONFIG_XMC4_I2C2_FREQUENCY;
    }
  else
#endif
#ifdef CONFIG_XMC4_I2C3
  if (channel == 3)
    {
      /* Set up I2C3 register base address and IRQ number */

      priv           = &g_i2c3;
      priv->base     = XMC4_USIC1_CH1_BASE;
      priv->irq      = XMC4_IRQ_USIC1_SR1;
      priv->i2cchan  = 3;
      priv->dx0      = BOARD_I2C3_DX0;
      priv->dx1      = BOARD_I2C3_DX1;

      /* Configure PIO pins */

      xmc4_gpio_config(GPIO_I2C3_SCL);
      xmc4_gpio_config(GPIO_I2C3_SDA);

      /* Select the I2C frequency */

      frequency  = CONFIG_XMC4_I2C3_FREQUENCY;
    }
  else
#endif
#ifdef CONFIG_XMC4_I2C4
  if (channel == 4)
    {
      /* Set up I2C4 register base address and IRQ number */

      priv           = &g_i2c4;
      priv->base     = XMC4_USIC2_CH0_BASE;
      priv->irq      = XMC4_IRQ_USIC2_SR0;
      priv->i2cchan  = 4;
      priv->dx0      = BOARD_I2C4_DX0;
      priv->dx1      = BOARD_I2C4_DX1;

      /* Configure PIO pins */

      xmc4_gpio_config(GPIO_I2C4_SCL);
      xmc4_gpio_config(GPIO_I2C4_SDA);

      /* Select the I2C frequency */

      frequency  = CONFIG_XMC4_I2C4_FREQUENCY;
    }
  else
#endif
#ifdef CONFIG_XMC4_I2C5
  if (channel == 5)
    {
      /* Set up I2C5 register base address and IRQ number */

      priv           = &g_i2c5;
      priv->base     = XMC4_USIC2_CH1_BASE;
      priv->irq      = XMC4_IRQ_USIC2_SR1;
      priv->i2cchan  = 5;
      priv->dx0      = BOARD_I2C5_DX0;
      priv->dx1      = BOARD_I2C5_DX1;

      /* Configure PIO pins */

      xmc4_gpio_config(GPIO_I2C5_SCL);
      xmc4_gpio_config(GPIO_I2C5_SDA);

      /* Select the I2C frequency */

      frequency  = CONFIG_XMC4_I2C5_FREQUENCY;
    }
  else
#endif
    {
      leave_critical_section(flags);
      i2cerr("ERROR: Unsupported bus: I2C%d\n", channel);
      return NULL;
    }

  /* Initialize the device structure */

  priv->dev.ops = &g_i2cops;

  /* Initialize semaphores */

  nxsem_init(&priv->exclsem, 0, 1);
  nxsem_init(&priv->waitsem, 0, 0);

  /* The waitsem semaphore is used for signaling and, hence, should not have
   * priority inheritance enabled.
   */

  nxsem_setprotocol(&priv->waitsem, SEM_PRIO_NONE);

  /* Allocate a watchdog timer */

  priv->timeout = wd_create();
  DEBUGASSERT(priv->timeout != 0);

  /* Configure and enable the I2C hardware */

  i2c_hw_initialize(priv, frequency);

  /* Attach Interrupt Handler */

  irq_attach(priv->irq, i2c_interrupt, priv);

  /* Enable Interrupts */

  up_enable_irq(priv->irq);
  leave_critical_section(flags);
  return &priv->dev;
}

/****************************************************************************
 * Name: xmc4_i2cbus_uninitialize
 *
 * Description:
 *   Uninitialise an I2C device
 *
 ****************************************************************************/

int xmc4_i2cbus_uninitialize(FAR struct i2c_master_s * dev)
{
  struct i2c_dev_s *priv = (struct i2c_dev_s *) dev;

  i2cinfo("I2C%d Un-initializing\n", priv->i2cchan);

  /* Disable interrupts */

  up_disable_irq(priv->irq);

  /* Reset data structures */

  nxsem_destroy(&priv->exclsem);
  nxsem_destroy(&priv->waitsem);

  /* Free the watchdog timer */

  wd_delete(priv->timeout);
  priv->timeout = NULL;

  /* Detach Interrupt Handler */

  irq_detach(priv->irq);
  return OK;
}

#endif /* CONFIG_XMC4_I2C0 || CONFIG_XMC4_I2C1 ... CONFIG_XMC4_I2C5 */
